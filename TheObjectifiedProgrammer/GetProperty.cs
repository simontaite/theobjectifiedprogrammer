﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TheObjectifiedProgrammer
{
    public static class GetProperty
    {
        public static string GetPropertyName<T>(this T type, Expression<Func<T, object>> expression)
        {
            return GetPropertyName<T>(expression);
        }

        public static string GetPropertyName<T>(Expression<Func<T, object>> expression)
        {
            var lambda = expression as LambdaExpression;

            MemberExpression memberExpression = lambda.Body is UnaryExpression
                                                    ? (lambda.Body as UnaryExpression).Operand as MemberExpression
                                                    : lambda.Body as MemberExpression;

            return memberExpression == null ? null : (memberExpression.Member as PropertyInfo).Name;
        }
    }
}
