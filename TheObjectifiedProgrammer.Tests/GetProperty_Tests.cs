﻿using System;
using System.Linq;
using System.Web.Mvc;

using NUnit.Framework;

using TheObjectifiedProgrammer;

namespace TheObjectifiedProgrammer.Tests
{
    [TestFixture]
    public class GetProperty_Tests
    {
        [Test]
        public void GetPropertyFromObject()
        {
            DateTime testObject = DateTime.Now;

            var result = testObject.GetPropertyName(t => t.Hour);

            Assert.IsNotNull(result);
            Assert.AreEqual("Hour", result);
        }

        [Test]
        public void GetPropertyFromType()
        {
            var result = GetProperty.GetPropertyName<TimeSpan>(t => t.Hours);

            Assert.IsNotNull(result);
            Assert.AreEqual("Hours", result);
        }

        [Test]
        public void GetProperty_CreateSelectList()
        {
            var items = Enumerable.Empty<ListItemTest>();

            var item = items.FirstOrDefault();
            var idName = item.GetPropertyName(i => i.ID);
            var valueName = item.GetPropertyName(i => i.Value);

            SelectList list = new SelectList(items, idName, valueName);

            Assert.IsNotNull(list);
        }

        #region Helpers

        public class ListItemTest
        {
            public int ID { get; set; }
            public string Value { get; set; }
        }

        #endregion
    }
}
